@extends('layouts.index')

@section('content')
    <div class="off-canvas-menu-bar">
        <div class="container">
            <div class="row">
                <div class="col-8 my-auto">
                    <a href="{{route('home')}}" class="custom-logo-link" rel="home" aria-current="page"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah"></a>
                </div>
                <div class="col-2 my-auto">
                    <div class="header-btn float-right">
                        <a class="my-account-btn" href="https://themebing.com/wp/tijarah/my-account/"> <img src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/images/user.png" alt="Home"> </a>
                    </div>
                </div>
                <div class="col-2 my-auto">
                    <div class="mobile-nav-toggler">
                        <span class="fas fa-bars"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu  -->
    <div class="off-canvas-menu">
        <div class="menu-backdrop"></div>
        <i class="close-btn fa fa-close"></i>
        <nav class="mobile-nav">
            <div class="text-center pt-3 pb-3">
                <a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home" aria-current="page"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah"></a>
            </div>
            <ul class="navigation">
                <!--Keep This Empty / Menu will come through Javascript-->
            </ul>
        </nav>
    </div>
    <div data-elementor-type="wp-page" data-elementor-id="70" class="elementor elementor-70" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-cc2dcc7 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="cc2dcc7" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-dc95104" data-id="dc95104" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-a212b16 elementor-widget elementor-widget-banner" data-id="a212b16" data-element_type="widget" data-widget_type="banner.default">
                                            <div class="elementor-widget-container">
                                                <section class="banner ">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-lg-7">
                                                                <div class="banner-content">
                                                                    <h1>Best Themes and Plugins Marketplace</h1>
                                                                    <p>Welcome to DigiMarket Multi vendor Marketplace Theme. Buy and Sell any kind of Digital Product you Wish. </p>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <ul class="list-inline banner-button">
                                                                    <li class="list-inline-item"> <a href="#">Chọn sản phẩm</a> </li>
                                                                    <li class="list-inline-item"> <a href="#">Dịch vụ</a> </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-f328f90 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="f328f90" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ce0fdde" data-id="ce0fdde" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-81211b7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="81211b7" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-740eda7" data-id="740eda7" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-372c9b8" data-id="372c9b8" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b438b3e elementor-widget elementor-widget-title" data-id="b438b3e" data-element_type="widget" data-widget_type="title.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="section-title text-center">
                                                                            <h1>Why Choose DigiMarket</h1>
                                                                            <p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-3a587f7 elementor-widget elementor-widget-spacer" data-id="3a587f7" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-783269e" data-id="783269e" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-dbc5633 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="dbc5633" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-extended">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2931051" data-id="2931051" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-9a7460d animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="9a7460d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-center" style="background: rgba(144, 19, 254, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fas fa-code"></i>
                                                                            <h5 style="color:#7D6E9B">We are Open Source</h5>
                                                                            <p style="color:#7D6E9B">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-155a658" data-id="155a658" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-6e91566 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="6e91566" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:400}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-center" style="background: rgba(43, 98, 201, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fas fa-lightbulb"></i>
                                                                            <h5 style="color:#707DAC">Problem Solvers</h5>
                                                                            <p style="color:#707DAC">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6c18dce" data-id="6c18dce" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-77a1501 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="77a1501" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:1000}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-center" style="background: rgba(26, 188, 156, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fab fa-superpowers"></i>
                                                                            <h5 style="color:#607E70">Regular Updates &amp; Bug fixes</h5>
                                                                            <p style="color:#607E70">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-9c5b553 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="8816cc7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c287882" data-id="c287882" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-e563495 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="e563495" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-35b4fa2" data-id="35b4fa2" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-303383c" data-id="303383c" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-d2a131a elementor-widget elementor-widget-title" data-id="d2a131a" data-element_type="widget" data-widget_type="title.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="section-title text-center">
                                                                            <h1>Pricing Plan</h1>
                                                                            <p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-fbee5a7 elementor-widget elementor-widget-spacer" data-id="fbee5a7" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-14a073e" data-id="14a073e" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-947844a elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="947844a" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-502514e" data-id="502514e" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-7f63a9b elementor-widget elementor-widget-pricing_woocommerce" data-id="7f63a9b" data-element_type="widget" data-widget_type="pricing_woocommerce.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="row justify-content-center">
                                                                            <div class="col-lg-4 col-md-6">
                                                                                <div class="tijarah-pricing-table">
                                                                                    <i aria-hidden="true" style="color:#fff; background-image: linear-gradient(180deg, #00e31d 0%, #00c0ce 180%);" class="fas fa-dice-d20"></i>
                                                                                    <h1 class="tijarah-price elementor-inline-editing"> <span>$</span> 39 </h1>
                                                                                    <h6>Basic</h6>
                                                                                    <ul>
                                                                                        <li>Demo Content Install</li>
                                                                                        <li>Theme Updates</li>
                                                                                        <li>Support And Updates</li>
                                                                                        <li>Access All Themes</li>
                                                                                        <li>All Themes For Life</li>
                                                                                    </ul>
                                                                                    <a class="tijarah-buy-button" href="https://themebing.com/wp/tijarah/cart/?add-to-cart=2325">Purchase Now</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-6">
                                                                                <div class="tijarah-pricing-table">
                                                                                    <i aria-hidden="true" style="color:#fff; background-image: linear-gradient(180deg, #ff6167 0%, #ff0e27 180%);" class="fas fa-crown"></i>
                                                                                    <h1 class="tijarah-price elementor-inline-editing"> <span>$</span> 59 </h1>
                                                                                    <h6>Standard</h6>
                                                                                    <ul>
                                                                                        <li>Demo Content Install</li>
                                                                                        <li>Theme Updates</li>
                                                                                        <li>Support And Updates</li>
                                                                                        <li>Access All Themes</li>
                                                                                        <li>All Themes For Life</li>
                                                                                    </ul>
                                                                                    <a class="tijarah-buy-button" href="https://themebing.com/wp/tijarah/cart/?add-to-cart=2326">Purchase Now</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-6">
                                                                                <div class="tijarah-pricing-table">
                                                                                    <i aria-hidden="true" style="color:#fff; background-image: linear-gradient(180deg, #00aeef 0%, #0f5ec9 180%);" class="fab fa-wordpress"></i>
                                                                                    <h1 class="tijarah-price elementor-inline-editing"> <span>$</span> 120 </h1>
                                                                                    <h6>Premium</h6>
                                                                                    <ul>
                                                                                        <li>Demo Content Install</li>
                                                                                        <li>Premium Support</li>
                                                                                        <li>Support And Updates</li>
                                                                                        <li>Access All Themes</li>
                                                                                        <li>All Themes For Life</li>
                                                                                    </ul>
                                                                                    <a class="tijarah-buy-button" href="https://themebing.com/wp/tijarah/cart/?add-to-cart=2328">Purchase Now</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-9f6daa8 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9f6daa8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-fca60b5" data-id="fca60b5" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-0d2c02d elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0d2c02d" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-293ad7b" data-id="293ad7b" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4ce5f2d" data-id="4ce5f2d" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-a2c89d8 elementor-widget elementor-widget-title" data-id="a2c89d8" data-element_type="widget" data-widget_type="title.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="section-title text-center">
                                                                            <h1>All Items</h1>
                                                                            <p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-eea4a2b elementor-widget elementor-widget-spacer" data-id="eea4a2b" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-efe6ac0" data-id="efe6ac0" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-0cc7f48 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0cc7f48" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-beab44b" data-id="beab44b" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-c864087 elementor-widget elementor-widget-download" data-id="c864087" data-element_type="widget" data-widget_type="download.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="container">
                                                                            <div class="download-filter">
                                                                                <ul class="list-inline">
                                                                                    <li class="select-cat list-inline-item product-filter" data-product-cat="site-templates,cms-themes,ecommerce,joomla,">All Items</li>
                                                                                    <li class="list-inline-item product-filter" data-product-cat="site-templates">Site Templates</li>
                                                                                    <li class="list-inline-item product-filter" data-product-cat="cms-themes">CMS Themes</li>
                                                                                    <li class="list-inline-item product-filter" data-product-cat="ecommerce">eCommerce</li>
                                                                                    <li class="list-inline-item product-filter" data-product-cat="joomla">Joomla</li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="loader"></div>
                                                                            <div class="download_items row justify-content-center">
                                                                                <div class="loader"></div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/2836341-750x430.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy"> </a>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/"> <h5>MyAgency</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>59.00</bdi></span> – <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>199.00</bdi></span></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 3.67 out of 5">
                                                                                                        <span style="width:73.4%">Rated <strong class="rating">3.67</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
{{--                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>--}}
                                                                                                <li> <a href="javascript:void(0)" class="btn-preview" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-info-circle"></i>Details</a> </li>
{{--                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/" data-quantity="1" class="button product_type_variable add_to_cart_button" data-product_id="2181" data-product_sku="" aria-label="Select options for “MyAgency”" rel="nofollow">Select options</a> </li>--}}
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/ecommerce/appso/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-750x430.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-750x430.jpg 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-500x286.jpg 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-350x200.jpg 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/ecommerce/appso/"> <h5>AppSO</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>39.00</bdi></span></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 4.67 out of 5">
                                                                                                        <span style="width:93.4%">Rated <strong class="rating">4.67</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/ecommerce/appso/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="?add-to-cart=2180" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2180" data-product_sku="" aria-label="Add “AppSO” to your cart" rel="nofollow">Add to cart</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/site-templates/munio/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                            <span class="onsale">Sale!</span>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/site-templates/munio/"> <h5>Munio</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><del><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>49.00</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>19.00</bdi></span></ins></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 3.00 out of 5">
                                                                                                        <span style="width:60%">Rated <strong class="rating">3.00</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/site-templates/munio/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="?add-to-cart=2176" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2176" data-product_sku="" aria-label="Add “Munio” to your cart" rel="nofollow">Add to cart</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/"> <h5>Benchmark</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>59.00</bdi></span> – <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>199.00</bdi></span></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 3.67 out of 5">
                                                                                                        <span style="width:73.4%">Rated <strong class="rating">3.67</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/" data-quantity="1" class="button product_type_variable add_to_cart_button" data-product_id="2177" data-product_sku="" aria-label="Select options for “Benchmark”" rel="nofollow">Select options</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/cms-themes/instive/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/cms-themes/instive/"> <h5>Instive</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>39.00</bdi></span></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 3.67 out of 5">
                                                                                                        <span style="width:73.4%">Rated <strong class="rating">3.67</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/cms-themes/instive/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="?add-to-cart=2178" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2178" data-product_sku="" aria-label="Add “Instive” to your cart" rel="nofollow">Add to cart</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/"> <h5>Avtorai</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>30.00</bdi></span></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 3.67 out of 5">
                                                                                                        <span style="width:73.4%">Rated <strong class="rating">3.67</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="?add-to-cart=2172" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2172" data-product_sku="" aria-label="Add “Avtorai” to your cart" rel="nofollow">Add to cart</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/ecommerce/birdily/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-750x430.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-750x430.jpg 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-300x171.jpg 300w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-500x286.jpg 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-350x200.jpg 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                            <span class="onsale">Sale!</span>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/ecommerce/birdily/"> <h5>Birdily</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><del><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>30.00</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>20.00</bdi></span></ins></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 3.67 out of 5">
                                                                                                        <span style="width:73.4%">Rated <strong class="rating">3.67</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/ecommerce/birdily/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="?add-to-cart=2174" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2174" data-product_sku="" aria-label="Add “Birdily” to your cart" rel="nofollow">Add to cart</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/cms-themes/fourmusic/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/cms-themes/fourmusic/"> <h5>Fourmusic</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>59.00</bdi></span></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 3.00 out of 5">
                                                                                                        <span style="width:60%">Rated <strong class="rating">3.00</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/cms-themes/fourmusic/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="?add-to-cart=2173" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2173" data-product_sku="" aria-label="Add “Fourmusic” to your cart" rel="nofollow">Add to cart</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Item -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="download-item">
                                                                                        <div class="download-item-image">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/site-templates/daeron/"> <img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px"> </a>
                                                                                            <span class="onsale">Sale!</span>
                                                                                        </div>
                                                                                        <div class="download-item-content">
                                                                                            <a href="https://themebing.com/wp/tijarah/shop/site-templates/daeron/"> <h5>Daeron</h5> </a>
                                                                                            <p>Photography WordPress Theme</p>
                                                                                            <ul class="list-inline mb-0">
                                                                                                <li class="list-inline-item"> <p class="price"><del><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>79.00</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span>59.00</bdi></span></ins></p> </li>
                                                                                                <li class="list-inline-item float-right">
                                                                                                    <div class="star-rating" role="img" aria-label="Rated 1.00 out of 5">
                                                                                                        <span style="width:20%">Rated <strong class="rating">1.00</strong> out of 5</span>
                                                                                                    </div></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="download-item-overlay">
                                                                                            <ul class="text-center mb-0 pl-0">
                                                                                                <li> <a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a> </li>
                                                                                                <li> <a href="https://themebing.com/wp/tijarah/shop/site-templates/daeron/"><i class="fa fa-info-circle"></i>Details</a> </li>
                                                                                                <li> <a href="?add-to-cart=2169" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2169" data-product_sku="" aria-label="Add “Daeron” to your cart" rel="nofollow">Add to cart</a> </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-979ec8c elementor-widget elementor-widget-button" data-id="979ec8c" data-element_type="widget" data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="text-center">
                                                                            <a class="tijarah-btn  elementor-inline-editing shadow " style="border-radius: 50px;" href="https://themebing.com/wp/tijarah/shop/"><i class="fa fa-cart-arrow-down"></i>More Products</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-9c5b553 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="690780e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-11368fb" data-id="11368fb" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-603e603 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="603e603" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3710f48" data-id="3710f48" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2cd958f" data-id="2cd958f" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-f87a6bc elementor-widget elementor-widget-title" data-id="f87a6bc" data-element_type="widget" data-widget_type="title.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="section-title text-center">
                                                                            <h1>DigiMarket Features</h1>
                                                                            <p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-e107ee6 elementor-widget elementor-widget-spacer" data-id="e107ee6" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8c78832" data-id="8c78832" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-955841f elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="955841f" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-extended">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-c9b1cda" data-id="c9b1cda" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-86ce87b animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="86ce87b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-left" style="background: rgba(144, 19, 254, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fas fa-code"></i>
                                                                            <h5 style="color:#7D6E9B">We are Open Source</h5>
                                                                            <p style="color:#7D6E9B">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-aa4e39c animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="aa4e39c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-left" style="background: rgba(233, 79, 68, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fas fa-puzzle-piece"></i>
                                                                            <h5 style="color:#866E7D">Feature-Rich Free &amp; Pro Plugins</h5>
                                                                            <p style="color:#866E7D">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-d6759bf" data-id="d6759bf" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-e3f9cd3 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="e3f9cd3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:400}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-left" style="background: rgba(43, 98, 201, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fas fa-lightbulb"></i>
                                                                            <h5 style="color:#707DAC">Problem Solvers</h5>
                                                                            <p style="color:#707DAC">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-0e55da7 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="0e55da7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:1000}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-left" style="background: rgba(26, 188, 156, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fab fa-superpowers"></i>
                                                                            <h5 style="color:#607E70">Regular Updates &amp; Bug fixes</h5>
                                                                            <p style="color:#607E70">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-83f5fad" data-id="83f5fad" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-f55702f animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="f55702f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-left" style="background: rgba(0, 130, 255, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fas fa-hands-helping"></i>
                                                                            <h5 style="color:#637595">Highly-Rated Support</h5>
                                                                            <p style="color:#637595">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-21bbcca animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="21bbcca" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:1200}" data-widget_type="InfoBox_item.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="infobox-item style-2 text-left" style="background: rgba(245, 166, 35, 0.06)">
                                                                            <i aria-hidden="true" style="color:#fff" class="fas fa-cog"></i>
                                                                            <h5 style="color:#897C66">Extensions to step-up your game</h5>
                                                                            <p style="color:#897C66">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-9f6daa8 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9c5b553" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-35e566b" data-id="35e566b" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-765c11b elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="765c11b" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8f1d7f9" data-id="8f1d7f9" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-31558b9" data-id="31558b9" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-89d1226 elementor-widget elementor-widget-title" data-id="89d1226" data-element_type="widget" data-widget_type="title.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="section-title text-center">
                                                                            <h1>Latest from Blog</h1>
                                                                            <p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8eb5e66 elementor-widget elementor-widget-spacer" data-id="8eb5e66" data-element_type="widget" data-widget_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-a58f854" data-id="a58f854" data-element_type="column">
                                                        <div class="elementor-column-wrap">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-1e0f706 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1e0f706" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d41038d" data-id="d41038d" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-1b5b5fa elementor-widget elementor-widget-blog" data-id="1b5b5fa" data-element_type="widget" data-widget_type="blog.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="container">
                                                                            <div class="row justify-content-center">
                                                                                <!-- blog -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="blog-item">
                                                                                        <div class="blog-thumb">
                                                                                            <a href="https://themebing.com/wp/tijarah/blog/20-best-free-html-marketplace-templates/"> <img src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/leone-venter-pVt9j3iWtPM-unsplash-350x200.jpg" alt="20+ Best Free HTML Marketplace Templates"> </a>
                                                                                        </div>
                                                                                        <div class="blog-content">
                                                                                            <div class="blog-meta">
                                                                                                <a href="https://themebing.com/wp/tijarah/blog/author/admin/">ThemeBing</a>
                                                                                                <span> - April 3, 2018</span>
                                                                                            </div>
                                                                                            <h4><a href="https://themebing.com/wp/tijarah/blog/20-best-free-html-marketplace-templates/">20+ Best Free HTML Marketplace Templates</a></h4>
                                                                                            <p>Marketing is the and management of exchange relationships. Marketing ...</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- blog -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="blog-item">
                                                                                        <div class="blog-thumb">
                                                                                            <a href="https://themebing.com/wp/tijarah/blog/best-free-responsive-wordpress-themes-in-2020/"> <img src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/john-mark-arnold-ti4kGLkGgmU-unsplash-350x200.jpg" alt="Best Free Responsive WordPress Themes in 2020"> </a>
                                                                                        </div>
                                                                                        <div class="blog-content">
                                                                                            <div class="blog-meta">
                                                                                                <a href="https://themebing.com/wp/tijarah/blog/author/admin/">ThemeBing</a>
                                                                                                <span> - May 11, 2018</span>
                                                                                            </div>
                                                                                            <h4><a href="https://themebing.com/wp/tijarah/blog/best-free-responsive-wordpress-themes-in-2020/">Best Free Responsive WordPress Themes in...</a></h4>
                                                                                            <p>Marketing is the and management of exchange relationships. Marketing ...</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- blog -->
                                                                                <div class="col-xl-4 col-md-6">
                                                                                    <div class="blog-item">
                                                                                        <div class="blog-thumb">
                                                                                            <a href="https://themebing.com/wp/tijarah/blog/top-web-design-trends-you-must-know-in-2020/"> <img src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-350x200.png" alt="Top Web Design Trends You Must Know in 2020"> </a>
                                                                                        </div>
                                                                                        <div class="blog-content">
                                                                                            <div class="blog-meta">
                                                                                                <a href="https://themebing.com/wp/tijarah/blog/author/admin/">ThemeBing</a>
                                                                                                <span> - June 12, 2018</span>
                                                                                            </div>
                                                                                            <h4><a href="https://themebing.com/wp/tijarah/blog/top-web-design-trends-you-must-know-in-2020/">Top Web Design Trends You Must...</a></h4>
                                                                                            <p>Marketing is the and management of exchange relationships. Marketing ...</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-0c42353 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="0c42353" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f0d6106" data-id="f0d6106" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-efa9302 elementor-widget elementor-widget-call_to_action" data-id="efa9302" data-element_type="widget" data-widget_type="call_to_action.default">
                                            <div class="elementor-widget-container">
                                                <section class="call-to-action">
                                                    <div class="container">
                                                        <div class="row justify-content-between">
                                                            <div class="col-md-7 text-left">
                                                                <h2>Join Us Today</h2>
                                                                <p class="mb-0">Over 75,000 designers and developers trust the DigiMarket.</p>
                                                            </div>
                                                            <div class="col-md-5 my-auto text-right">
                                                                <a class="tijarah-btn" href="#">Join Us Today</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <iframe src="https://themebing.com/wp/tijarah/home-2/" title="iframe" style="width: 100%; height: 1000%"></iframe>
                </div>

            </div>
        </div>
    </div>
@endsection
