<footer id="colophon" class="site-footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row justify-content-xl-between">
                <div class="col-lg-4">
                    <div id="custom_html-2" class="widget_text footer-widget widget_custom_html">
                        <div class="textwidget custom-html-widget">
                            <div class="pr-5">
                                <img width="140" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="img-fluid mb-4" alt="Tijarah">
                                <div class="footer-text mb-4">
                                    <p>Popularised in the with the release of etras sheets containing passages and more rcently with desop publishing software like Maker including.</p>
                                </div>
                                <div class="footer-social">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-2" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Products</h5>
                        <div class="menu-help-support-container">
                            <ul id="menu-help-support" class="menu">
                                <li id="menu-item-2271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2271"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li id="menu-item-2273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2273"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li id="menu-item-2275" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2275"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                                <li id="menu-item-2272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2272"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
                                <li id="menu-item-2274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2274"><a href="https://themebing.com/wp/tijarah/shop/">Plugins</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-3" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Resources</h5>
                        <div class="menu-our-company-container">
                            <ul id="menu-our-company" class="menu">
                                <li id="menu-item-2262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2262"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li id="menu-item-2268" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2268"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li id="menu-item-2270" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2270"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a></li>
                                <li id="menu-item-2266" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2266"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
                                <li id="menu-item-2267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2267"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-4" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Company</h5>
                        <div class="menu-help-support-container">
                            <ul id="menu-help-support-1" class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2271"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2273"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2275"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2272"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2274"><a href="https://themebing.com/wp/tijarah/shop/">Plugins</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-5" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Help and FAQs</h5>
                        <div class="menu-our-company-container">
                            <ul id="menu-our-company-1" class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2262"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2268"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2270"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2266"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2267"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-bar">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-7 text-center">
                    <p> </p>
                    <p>Copyright © 2020 tijarah All Rights Reserved.</p>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</footer>
