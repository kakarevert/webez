<header class="site-header sticky-header">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-2 col-md-3">
                <div class="logo">
                    <a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home" aria-current="page"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah"></a>
                </div>
            </div>
            <div class="col-xl-10 col-md-9">
                <div class="primary-menu d-none d-lg-inline-block float-right">
                    <nav class="desktop-menu">
                        <ul id="menu-primary" class="menu">
                            <li id="menu-item-290" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290"><a href="javascript:void(0)">Theme</a></li>
                            <li id="menu-item-290" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290"><a href="javascript:void(0)">Service</a></li>
                            <li id="menu-item-291" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291"><a href="javascript:void(0)">Pricing Plan</a></li>
                            <li id="menu-item-292" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292"><a href="javascript:void(0)">Contact</a></li>
                            <li id="menu-item-294" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-294"><a href="javascript:void(0)">Faq</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</header>
