<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://gmpg.org/xfn/11">
    <title>Easy Website</title>
    <link rel="stylesheet" href="//fonts.googleapis.com">
    <link rel="stylesheet" href="//s.w.org">
    <link rel="stylesheet" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" type="application/rss+xml" title="Tijarah » Feed" href="https://themebing.com/wp/tijarah/feed/">
    <link rel="stylesheet" type="application/rss+xml" title="Tijarah » Comments Feed" href="https://themebing.com/wp/tijarah/comments/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/themebing.com\/wp\/tijarah\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
        !function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css" href="{{ URL::asset('css/style.min.css?ver=5.5.3') }}" type="text/css" media="all">
    <link rel="stylesheet" id="wc-block-vendors-style-css" href="{{ URL::asset('css/vendors-style.css?ver=3.8.1') }}" type="text/css" media="all">
    <link rel="stylesheet" id="wc-block-style-css" href="{{ URL::asset('css/style.css?ver=3.8.1') }}" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="{{ URL::asset('css/styles.css?ver=5.3.2') }}" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugns-css" href="{{ URL::asset('css/plugins.css?ver=5.5.3') }}" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugn-css" href="{{ URL::asset('css/plugin.css?ver=5.5.3') }}" type="text/css" media="all">
    <link rel="stylesheet" id="wp-block-library-css" href="https://themebing.com/wp/tijarah/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="wc-block-vendors-style-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=3.8.1" type="text/css" media="all">
    <link rel="stylesheet" id="wc-block-style-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=3.8.1" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugns-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/css/plugins.css?ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugn-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/css/plugin.css?ver=5.5.3" type="text/css" media="all">
    <style id="woocommerce-inline-inline-css" type="text/css">
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel="stylesheet" id="tijarah-fonts-css" href="//fonts.googleapis.com/css?family=Rubik%3A300%2C400%2C500%2C700%2C900%26display%3Dswap&amp;ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugin-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/css/plugin.css?ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-style-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/style.css?ver=5.5.3" type="text/css" media="all">
    <style id="tijarah-style-inline-css" type="text/css">

        .preview-btn li a:hover,
        .call-to-action,
        #backtotop i,
        .comment-navigation .nav-links a,
        blockquote:before,
        .mean-container .mean-nav ul li a.mean-expand:hover,
        button, input[type="button"],
        .widget_price_filter .ui-slider .ui-slider-range,
        .widget_price_filter .ui-slider .ui-slider-handle,
        input[type="reset"],
        .off-canvas-menu .navigation li>a:hover,
        .off-canvas-menu .navigation .dropdown-btn:hover,
        .off-canvas-menu .navigation li .cart-contents,
        input[type="submit"],
        .tijarah-search-btn,
        .video-item .view-detail,
        .woocommerce-store-notice .woocommerce-store-notice__dismiss-link,
        .widget-product-details .widget-add-to-cart .variations .value .variation-radios [type="radio"]:checked + label:after,
        .widget-product-details .widget-add-to-cart .variations .value .variation-radios [type="radio"]:not(:checked) + label:after,
        .plyr__control--overlaid,
        .plyr--video .plyr__control.plyr__tab-focus,
        .plyr--video .plyr__control:hover,
        .plyr--video .plyr__control[aria-expanded=true],
        .product-social-share .float,
        .banner2 .banner-cat .cat-count,
        ul.banner-button li:first-child a,
        ul.banner-button li a:hover,
        .tijarah-pricing-table.recommended,
        .tijarah-pricing-table a:hover,
        .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_parent > a, .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_item > a, .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_ancestor > a,
        .primary-menu ul li .children li.current-menu-item>a,
        .primary-menu ul li .sub-menu li.current-menu-item>a,
        .header-btn .sub-menu li.is-active a,
        .download-item-button a:hover,
        .recent-themes-widget,
        .newest-filter ul li.select-cat,
        .download-filter ul li.select-cat,
        .woocommerce .onsale,
        .download-item-overlay ul a:hover,
        .download-item-overlay ul a.active,
        input[type="button"],
        input[type="reset"],
        input[type="submit"],
        .checkout-button,
        .woocommerce-tabs ul.tabs li.active a:after,
        .tagcloud a:hover,
        .tijarah-btn,
        .dokan-btn,
        a.dokan-btn,
        .dokan-btn-theme,
        input[type="submit"].dokan-btn-theme,
        .tijarah-btn.bordered:hover,
        .testimonials-nav .slick-arrow:hover,
        .widget-woocommerce .single_add_to_cart_button,
        .post-navigation .nav-previous a ,
        .post-navigation .nav-next a,
        .blog-btn .btn:hover,
        .mean-container .mean-nav,
        .recent-theme-item .permalink,
        .banner-item-btn a,
        .meta-attributes li span a:hover,
        .theme-item-price span,
        .error-404 a,
        .mini-cart .widget_shopping_cart .woocommerce-mini-cart__buttons a,
        .download-item-image .onsale,
        .theme-item-btn a:hover,
        .theme-banner-btn a,
        .comment-list .comment-reply-link,
        .comment-form input[type=submit],
        .pagination .nav-links .page-numbers.current,
        .pagination .nav-links .page-numbers:hover,
        .breadcrumb-banner,
        .children li a:hover,
        .excerpt-date,
        .widget-title:after,
        .widget-title:before,
        .primary-menu ul li .sub-menu li a:hover,
        .header-btn .sub-menu li a:hover,
        .photo-product-item .add_to_cart_button,
        .photo-product-item .added_to_cart,
        .tags a:hover,
        .playerContainer .seekBar .outer .inner,
        .playerContainer .volumeControl .outer .inner,
        .excerpt-readmore a {
            background: #FF416C;
            background: -webkit-linear-gradient(to right, #FF416C, #FF4B2B);
            background: linear-gradient(to right, #FF416C, #FF4B2B);
        }

        .mini-cart .cart-contents:hover span,
        ul.banner-button li a,
        .testimonial-content>i,
        .testimonials-nav .slick-arrow,
        .tijarah-btn.bordered,
        .header-btn .my-account-btn,
        .primary-menu ul li.current-menu-item>a,
        .cat-links a,
        .plyr--full-ui input[type=range],
        .tijarah-team-social li a,
        .preview-btn li a,
        .related-post-title a:hover,
        .comment-author-link,
        .entry-meta ul li a:hover,
        .widget-product-details table td span a:hover,
        .woocommerce-message a,
        .woocommerce-info a,
        .footer-widget ul li a:hover,
        .woocommerce-noreviews a,
        .widget li a:hover,
        p.no-comments a,
        .woocommerce-notices-wrapper a,
        .woocommerce table td a,
        .blog-meta span,
        .blog-content h4:hover a,
        .tags-links a,
        .tags a,
        .woocommerce-account .woocommerce-MyAccount-navigation li.is-active a,
        .navbar-logo-text,
        .docs-single h4 a:hover,
        .docs-single ul li a:hover,
        .navbar .menu-item>.active,
        blockquote::before,
        .woocommerce-tabs ul.tabs li.active a,
        .woocommerce-tabs ul.tabs li a:hover,
        .primary-menu ul li>a:hover,
        .the_excerpt .entry-title a:hover{
            color: #FF416C;
        }


        .tijarah-btn.bordered,
        ul.banner-button li a,
        .testimonials-nav .slick-arrow,
        .my-account-btn,
        .widget-title,
        .preview-btn li a,
        .woocommerce-info,
        .download-item-overlay ul a:hover,
        .download-item-overlay ul a.active,
        .tijarah-pricing-table a,
        .woocommerce-MyAccount-navigation .is-active a,
        blockquote,
        .testimonials-nav .slick-arrow:hover,
        .loader,
        .uil-ripple-css div,
        .uil-ripple-css div:nth-of-type(1),
        .uil-ripple-css div:nth-of-type(2),
        .related-themes .single-related-theme:hover,
        .theme-author span,
        .tags a,
        .playerContainer,
        .sticky .the_excerpt_content {
            border-color: #FF416C!important;
        }


        .navbar-toggler-icon {
            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='#FF416C' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
        }

        /*----------------------------------------
        IF SCREEN SIZE LESS THAN 769px WIDE
        ------------------------------------------*/

        @media screen and (max-width: 768px) {
            .navbar .menu-item>.active {
                background: #FF416C;
            }
        }

    </style>
    <link rel="stylesheet" id="tijarah-woocommerce-style-css" href="{{ URL::asset('css/woocommerce.css?ver=5.5.3') }}" type="text/css" media="all">
    <style id="tijarah-woocommerce-style-inline-css" type="text/css">
        @font-face {
            font-family: "star";
            src: url("fonts/star.eot");
            src: url("fonts/star_1.eot") format("embedded-opentype"),
            url("fonts/star.woff") format("woff"),
            url("fonts/star.ttf") format("truetype"),
            url("fonts/star.svg#star") format("svg");
            font-weight: normal;
            font-style: normal;
        }
    </style>
    <link rel="stylesheet" id="elementor-icons-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.9.1" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-frontend-legacy-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/css/frontend-legacy.min.css?ver=3.0.14" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-frontend-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.0.14" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-post-2251-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/post-2251.css?ver=1606322576" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-5-all-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css?ver=3.0.14" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-4-shim-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css?ver=3.0.14" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-global-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/global.css?ver=1606322576" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-post-70-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/post-70.css?ver=1606322576" type="text/css" media="all">
    <link rel="stylesheet" as="style" href="https://fonts.googleapis.com/css?family=Rubik:700&amp;display=swap&amp;ver=1604425745">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:700&amp;display=swap&amp;ver=1604425745" media="print" onload="this.media='all'"> <noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:700&amp;display=swap&amp;ver=1604425745">
    </noscript>
    <link rel="stylesheet" id="google-fonts-1-css" href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-icons-shared-0-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.12.0" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-icons-fa-solid-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.12.0" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-icons-fa-brands-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.12.0" type="text/css" media="all">
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" id="jquery-core-js"></script>
    <script type="text/javascript" id="tijarah_thumb_product_ajax_script-js-extra">
        /* <![CDATA[ */
        var tijarah_ajax_thumb_products_obj = {"tijarah_thumb_product_ajax_nonce":"5653dd4f39","tijarah_thumb_product_ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/ajax-woo-thumb-products/ajax.js" id="tijarah_thumb_product_ajax_script-js"></script>
    <script type="text/javascript" id="tijarah_product_ajax_script-js-extra">
        /* <![CDATA[ */
        var tijarah_ajax_products_obj = {"tijarah_product_ajax_nonce":"0fc703f452","tijarah_product_ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/ajax-woo-products/ajax.js" id="tijarah_product_ajax_script-js"></script>
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min.js?ver=3.0.14" id="font-awesome-4-shim-js"></script>
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-json/">
    <link rel="stylesheet" type="application/json" href="https://themebing.com/wp/tijarah/wp-json/wp/v2/pages/70">
    <link rel="stylesheet" type="application/rsd+xml" title="RSD" href="https://themebing.com/wp/tijarah/xmlrpc.php?rsd">
    <link rel="stylesheet" type="application/wlwmanifest+xml" href="https://themebing.com/wp/tijarah/wp-includes/wlwmanifest.xml">
    <meta name="generator" content="WordPress 5.5.3">
    <meta name="generator" content="WooCommerce 4.8.0">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/">
    <link rel="stylesheet" type="application/json+oembed" href="https://themebing.com/wp/tijarah/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemebing.com%2Fwp%2Ftijarah%2F">
    <link rel="stylesheet" type="text/xml+oembed" href="https://themebing.com/wp/tijarah/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemebing.com%2Fwp%2Ftijarah%2F&amp;format=xml">
    <meta name="framework" content="Redux 4.1.24">
    <!-- Starting: WooCommerce Conversion Tracking (https://wordpress.org/plugins/woocommerce-conversion-tracking/) -->
    <!-- End: WooCommerce Conversion Tracking Codes -->
    <noscript>
        <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
    </noscript>
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail-32x32.png" sizes="32x32">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png" sizes="192x192">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png">
    <meta name="msapplication-TileImage" content="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png">
    <style id="tijarah_opt-dynamic-css" title="dynamic-css" class="redux-options-output">h1,h2,h3,h4,h5,h6{font-family:Rubik;font-weight:700;font-style:normal;color:#333;font-display:swap;}body,p{font-family:Rubik;line-height:26px;font-weight:normal;font-style:normal;color:#808080;font-size:16px;font-display:swap;}.breadcrumb-banner{background-image:url('https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/breadcrumb.jpg');}.breadcrumb-banner h1,.breadcrumbs ul li{color:#FFFFFF;}</style>
</head>
<body class="home page-template page-template-custom-homepage page-template-custom-homepage-php page page-id-70 wp-custom-logo theme-tijarah woocommerce-no-js woocommerce-active elementor-default elementor-kit-2251 elementor-page elementor-page-70">
<div id="preloader">
    <div class="spinner">
        <div class="uil-ripple-css" style="transform:scale(0.29);">
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
@include('layouts.header')
<!-- #masthead -->
<!--Mobile Navigation Toggler-->
@yield('content')
@include('layouts.footer')
<!--======= Back to Top =======-->
<div id="backtotop">
    <i class="fa fa-lg fa-arrow-up"></i>
</div>
<script type="text/javascript">
    function fetch(){
        jQuery.ajax({
            url: 'https://themebing.com/wp/tijarah/wp-admin/admin-ajax.php',
            type: 'post',
            data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
            success: function(data) {
                if (jQuery('#keyword').val().length !== 0) {
                    jQuery('#datafetch').html( data );
                } else {
                    jQuery('#datafetch').html( '' );
                }

            }
        });

        jQuery("#datafetch").show();
    }
</script>
<script type="text/javascript">
    (function () {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })()
</script>
<script type="text/javascript" id="contact-form-7-js-extra">
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"https:\/\/themebing.com\/wp\/tijarah\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" id="contact-form-7-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/js/plugins.js?ver=1.2.7" id="tijarah-plugins-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/js/plugin.js?ver=1.2.7" id="tijarah-plugin-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70" id="jquery-blockui-js"></script>
<script type="text/javascript" id="wc-add-to-cart-js-extra">
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/themebing.com\/wp\/tijarah\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.8.0" id="wc-add-to-cart-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4" id="js-cookie-js"></script>
<script type="text/javascript" id="woocommerce-js-extra">
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.8.0" id="woocommerce-js"></script>
<script type="text/javascript" id="wc-cart-fragments-js-extra">
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_ad5ba321e4aaaebc185a7ab437321a4b","fragment_name":"wc_fragments_ad5ba321e4aaaebc185a7ab437321a4b","request_timeout":"5000"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.8.0" id="wc-cart-fragments-js"></script>
<script type="text/javascript" id="mailchimp-woocommerce-js-extra">
    /* <![CDATA[ */
    var mailchimp_public_data = {"site_url":"https:\/\/themebing.com\/wp\/tijarah","ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php","language":"en"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min.js?ver=2.5.0" id="mailchimp-woocommerce-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/popper.min.js?ver=1.2.7" id="popper-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/bootstrap.min.js?ver=1.2.7" id="bootstrap-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/jquery.nice-select.min.js?ver=1.2.7" id="nice-select-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/plyr.min.js?ver=1.2.7" id="plyr-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/audio-player.js?ver=1.2.7" id="tijarah-audio-player-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/jquery.magnific-popup.min.js?ver=1.2.7" id="magnific-popup-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/skip-link-focus-fix.js?ver=1.2.7" id="tijarah-skip-link-focus-fix-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/main.js?ver=1.2.7" id="tijarah-main-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/wp-embed.min.js?ver=5.5.3" id="wp-embed-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.0.14" id="elementor-frontend-modules-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4" id="jquery-ui-position-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.8.1" id="elementor-dialog-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2" id="elementor-waypoints-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6" id="swiper-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.0.14" id="share-link-js"></script>
<script type="text/javascript" id="elementor-frontend-js-before">
    var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.14","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"https:\/\/themebing.com\/wp\/tijarah\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":70,"title":"Tijarah%20%E2%80%93%20Multi-Vendor%20Digital%20Marketplace%20Theme","excerpt":"","featuredImage":false}};
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.0.14" id="elementor-frontend-js"></script>
</body>
</html>
