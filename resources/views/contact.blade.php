<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://gmpg.org/xfn/11">
    <title>Contact – Tijarah</title>
    <link rel="stylesheet" href="//fonts.googleapis.com">
    <link rel="stylesheet" href="//s.w.org">
    <link rel="stylesheet" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" type="application/rss+xml" title="Tijarah » Feed" href="https://themebing.com/wp/tijarah/feed/">
    <link rel="stylesheet" type="application/rss+xml" title="Tijarah » Comments Feed" href="https://themebing.com/wp/tijarah/comments/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/themebing.com\/wp\/tijarah\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
        !function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css" href="https://themebing.com/wp/tijarah/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="wc-block-vendors-style-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=3.8.1" type="text/css" media="all">
    <link rel="stylesheet" id="wc-block-style-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=3.8.1" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugns-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/css/plugins.css?ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugn-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/css/plugin.css?ver=5.5.3" type="text/css" media="all">
    <style id="woocommerce-inline-inline-css" type="text/css">
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel="stylesheet" id="tijarah-fonts-css" href="//fonts.googleapis.com/css?family=Rubik%3A300%2C400%2C500%2C700%2C900%26display%3Dswap&amp;ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-plugin-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/css/plugin.css?ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="tijarah-style-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/style.css?ver=5.5.3" type="text/css" media="all">
    <style id="tijarah-style-inline-css" type="text/css">

        .preview-btn li a:hover,
        .call-to-action,
        #backtotop i,
        .comment-navigation .nav-links a,
        blockquote:before,
        .mean-container .mean-nav ul li a.mean-expand:hover,
        button, input[type="button"],
        .widget_price_filter .ui-slider .ui-slider-range,
        .widget_price_filter .ui-slider .ui-slider-handle,
        input[type="reset"],
        .off-canvas-menu .navigation li>a:hover,
        .off-canvas-menu .navigation .dropdown-btn:hover,
        .off-canvas-menu .navigation li .cart-contents,
        input[type="submit"],
        .tijarah-search-btn,
        .video-item .view-detail,
        .woocommerce-store-notice .woocommerce-store-notice__dismiss-link,
        .widget-product-details .widget-add-to-cart .variations .value .variation-radios [type="radio"]:checked + label:after,
        .widget-product-details .widget-add-to-cart .variations .value .variation-radios [type="radio"]:not(:checked) + label:after,
        .plyr__control--overlaid,
        .plyr--video .plyr__control.plyr__tab-focus,
        .plyr--video .plyr__control:hover,
        .plyr--video .plyr__control[aria-expanded=true],
        .product-social-share .float,
        .banner2 .banner-cat .cat-count,
        ul.banner-button li:first-child a,
        ul.banner-button li a:hover,
        .tijarah-pricing-table.recommended,
        .tijarah-pricing-table a:hover,
        .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_parent > a, .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_item > a, .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_ancestor > a,
        .primary-menu ul li .children li.current-menu-item>a,
        .primary-menu ul li .sub-menu li.current-menu-item>a,
        .header-btn .sub-menu li.is-active a,
        .download-item-button a:hover,
        .recent-themes-widget,
        .newest-filter ul li.select-cat,
        .download-filter ul li.select-cat,
        .woocommerce .onsale,
        .download-item-overlay ul a:hover,
        .download-item-overlay ul a.active,
        input[type="button"],
        input[type="reset"],
        input[type="submit"],
        .checkout-button,
        .woocommerce-tabs ul.tabs li.active a:after,
        .tagcloud a:hover,
        .tijarah-btn,
        .dokan-btn,
        a.dokan-btn,
        .dokan-btn-theme,
        input[type="submit"].dokan-btn-theme,
        .tijarah-btn.bordered:hover,
        .testimonials-nav .slick-arrow:hover,
        .widget-woocommerce .single_add_to_cart_button,
        .post-navigation .nav-previous a ,
        .post-navigation .nav-next a,
        .blog-btn .btn:hover,
        .mean-container .mean-nav,
        .recent-theme-item .permalink,
        .banner-item-btn a,
        .meta-attributes li span a:hover,
        .theme-item-price span,
        .error-404 a,
        .mini-cart .widget_shopping_cart .woocommerce-mini-cart__buttons a,
        .download-item-image .onsale,
        .theme-item-btn a:hover,
        .theme-banner-btn a,
        .comment-list .comment-reply-link,
        .comment-form input[type=submit],
        .pagination .nav-links .page-numbers.current,
        .pagination .nav-links .page-numbers:hover,
        .breadcrumb-banner,
        .children li a:hover,
        .excerpt-date,
        .widget-title:after,
        .widget-title:before,
        .primary-menu ul li .sub-menu li a:hover,
        .header-btn .sub-menu li a:hover,
        .photo-product-item .add_to_cart_button,
        .photo-product-item .added_to_cart,
        .tags a:hover,
        .playerContainer .seekBar .outer .inner,
        .playerContainer .volumeControl .outer .inner,
        .excerpt-readmore a {
            background: #FF416C;
            background: -webkit-linear-gradient(to right, #FF416C, #FF4B2B);
            background: linear-gradient(to right, #FF416C, #FF4B2B);
        }

        .mini-cart .cart-contents:hover span,
        ul.banner-button li a,
        .testimonial-content>i,
        .testimonials-nav .slick-arrow,
        .tijarah-btn.bordered,
        .header-btn .my-account-btn,
        .primary-menu ul li.current-menu-item>a,
        .cat-links a,
        .plyr--full-ui input[type=range],
        .tijarah-team-social li a,
        .preview-btn li a,
        .related-post-title a:hover,
        .comment-author-link,
        .entry-meta ul li a:hover,
        .widget-product-details table td span a:hover,
        .woocommerce-message a,
        .woocommerce-info a,
        .footer-widget ul li a:hover,
        .woocommerce-noreviews a,
        .widget li a:hover,
        p.no-comments a,
        .woocommerce-notices-wrapper a,
        .woocommerce table td a,
        .blog-meta span,
        .blog-content h4:hover a,
        .tags-links a,
        .tags a,
        .woocommerce-account .woocommerce-MyAccount-navigation li.is-active a,
        .navbar-logo-text,
        .docs-single h4 a:hover,
        .docs-single ul li a:hover,
        .navbar .menu-item>.active,
        blockquote::before,
        .woocommerce-tabs ul.tabs li.active a,
        .woocommerce-tabs ul.tabs li a:hover,
        .primary-menu ul li>a:hover,
        .the_excerpt .entry-title a:hover{
            color: #FF416C;
        }


        .tijarah-btn.bordered,
        ul.banner-button li a,
        .testimonials-nav .slick-arrow,
        .my-account-btn,
        .widget-title,
        .preview-btn li a,
        .woocommerce-info,
        .download-item-overlay ul a:hover,
        .download-item-overlay ul a.active,
        .tijarah-pricing-table a,
        .woocommerce-MyAccount-navigation .is-active a,
        blockquote,
        .testimonials-nav .slick-arrow:hover,
        .loader,
        .uil-ripple-css div,
        .uil-ripple-css div:nth-of-type(1),
        .uil-ripple-css div:nth-of-type(2),
        .related-themes .single-related-theme:hover,
        .theme-author span,
        .tags a,
        .playerContainer,
        .sticky .the_excerpt_content {
            border-color: #FF416C!important;
        }


        .navbar-toggler-icon {
            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='#FF416C' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
        }

        /*----------------------------------------
        IF SCREEN SIZE LESS THAN 769px WIDE
        ------------------------------------------*/

        @media screen and (max-width: 768px) {
            .navbar .menu-item>.active {
                background: #FF416C;
            }
        }

    </style>
    <link rel="stylesheet" id="tijarah-woocommerce-style-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/css/woocommerce.css?ver=5.5.3" type="text/css" media="all">
    <style id="tijarah-woocommerce-style-inline-css" type="text/css">
        @font-face {
            font-family: "star";
            src: url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.eot");
            src: url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.eot?#iefix") format("embedded-opentype"),
            url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.woff") format("woff"),
            url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.ttf") format("truetype"),
            url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.svg#star") format("svg");
            font-weight: normal;
            font-style: normal;
        }
    </style>
    <link rel="stylesheet" id="elementor-icons-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.9.1" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-frontend-legacy-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/css/frontend-legacy.min.css?ver=3.0.15" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-frontend-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.0.15" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-post-2251-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/post-2251.css?ver=1608569439" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-5-all-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css?ver=3.0.15" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-4-shim-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css?ver=3.0.15" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-global-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/global.css?ver=1608569439" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-post-283-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/post-283.css?ver=1608601105" type="text/css" media="all">
    <link rel="stylesheet" as="style" href="https://fonts.googleapis.com/css?family=Rubik:700&amp;display=swap&amp;ver=1604425745">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:700&amp;display=swap&amp;ver=1604425745" media="print" onload="this.media='all'">
    <noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:700&amp;display=swap&amp;ver=1604425745">
    </noscript>
    <link rel="stylesheet" id="google-fonts-1-css" href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.5.3" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-icons-shared-0-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.12.0" type="text/css" media="all">
    <link rel="stylesheet" id="elementor-icons-fa-solid-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.12.0" type="text/css" media="all">
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" id="jquery-core-js"></script>
    <script type="text/javascript" id="tijarah_thumb_product_ajax_script-js-extra">
        /* <![CDATA[ */
        var tijarah_ajax_thumb_products_obj = {"tijarah_thumb_product_ajax_nonce":"691a4c0784","tijarah_thumb_product_ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/ajax-woo-thumb-products/ajax.js" id="tijarah_thumb_product_ajax_script-js"></script>
    <script type="text/javascript" id="tijarah_product_ajax_script-js-extra">
        /* <![CDATA[ */
        var tijarah_ajax_products_obj = {"tijarah_product_ajax_nonce":"496c512e1b","tijarah_product_ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/ajax-woo-products/ajax.js" id="tijarah_product_ajax_script-js"></script>
    <script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min.js?ver=3.0.15" id="font-awesome-4-shim-js"></script>
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-json/">
    <link rel="stylesheet" type="application/json" href="https://themebing.com/wp/tijarah/wp-json/wp/v2/pages/283">
    <link rel="stylesheet" type="application/rsd+xml" title="RSD" href="https://themebing.com/wp/tijarah/xmlrpc.php?rsd">
    <link rel="stylesheet" type="application/wlwmanifest+xml" href="https://themebing.com/wp/tijarah/wp-includes/wlwmanifest.xml">
    <meta name="generator" content="WordPress 5.5.3">
    <meta name="generator" content="WooCommerce 4.8.0">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/contact/">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/?p=283">
    <link rel="stylesheet" type="application/json+oembed" href="https://themebing.com/wp/tijarah/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemebing.com%2Fwp%2Ftijarah%2Fcontact%2F">
    <link rel="stylesheet" type="text/xml+oembed" href="https://themebing.com/wp/tijarah/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemebing.com%2Fwp%2Ftijarah%2Fcontact%2F&amp;format=xml">
    <meta name="framework" content="Redux 4.1.24">
    <!-- Starting: WooCommerce Conversion Tracking (https://wordpress.org/plugins/woocommerce-conversion-tracking/) -->
    <!-- End: WooCommerce Conversion Tracking Codes -->
    <noscript>
        <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
    </noscript>
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail-32x32.png" sizes="32x32">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png" sizes="192x192">
    <link rel="stylesheet" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png">
    <meta name="msapplication-TileImage" content="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png">
    <style id="tijarah_opt-dynamic-css" title="dynamic-css" class="redux-options-output">h1,h2,h3,h4,h5,h6{font-family:Rubik;font-weight:700;font-style:normal;color:#333;font-display:swap;}body,p{font-family:Rubik;line-height:26px;font-weight:normal;font-style:normal;color:#808080;font-size:16px;font-display:swap;}.breadcrumb-banner{background-image:url('https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/breadcrumb.jpg');}.breadcrumb-banner h1,.breadcrumbs ul li{color:#FFFFFF;}</style>
</head>
<body class="page-template page-template-custom-page page-template-custom-page-php page page-id-283 wp-custom-logo theme-tijarah woocommerce-no-js woocommerce-active elementor-default elementor-kit-2251 elementor-page elementor-page-283">
<!-- Preloading -->
<div id="preloader">
    <div class="spinner">
        <div class="uil-ripple-css" style="transform:scale(0.29);">
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header sticky-header">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-2 col-md-3">
                <div class="logo">
                    <a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah"></a>
                </div>
            </div>
            <div class="col-xl-8 col-md-9">
                <div class="primary-menu d-none d-lg-inline-block float-right">
                    <nav class="desktop-menu">
                        <ul id="menu-primary" class="menu">
                            <li id="menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-has-children menu-item-1971"><a href="https://themebing.com/wp/tijarah/">Home</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-282"><a href="https://themebing.com/wp/tijarah/">Home 1</a></li>
                                    <li id="menu-item-1970" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1970"><a href="https://themebing.com/wp/tijarah/home-2/">Home 2</a></li>
                                    <li id="menu-item-2385" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2385"><a href="https://themebing.com/wp/tijarah/videos/">Home 3 ( Video )</a></li>
                                    <li id="menu-item-2386" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2386"><a href="https://themebing.com/wp/tijarah/images">Home 4 ( Stock Photography )</a></li>
                                    <li id="menu-item-2387" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2387"><a href="https://themebing.com/wp/tijarah/audio/">Home 5 ( Audio )</a></li>
                                </ul> </li>
                            <li id="menu-item-289" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289"><a href="https://themebing.com/wp/tijarah/about/">About</a></li>
                            <li id="menu-item-285" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-285"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-2447" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2447"><a href="https://themebing.com/wp/tijarah/shop/">Shop Full Width</a></li>
                                    <li id="menu-item-2445" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2445"><a href="https://themebing.com/wp/tijarah/shop/?shop_layout=left_sidebar">Shop Left Sidebar</a></li>
                                    <li id="menu-item-2446" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2446"><a href="https://themebing.com/wp/tijarah/shop/?shop_layout=right_sidebar">Shop Right Sidebar</a></li>
                                </ul> </li>
                            <li id="menu-item-2317" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2317"><a href="#">Pages</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-2357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2357"><a href="https://themebing.com/wp/tijarah/services/">Services</a></li>
                                    <li id="menu-item-2370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2370"><a href="https://themebing.com/wp/tijarah/pricing-plan/">Pricing Plan</a></li>
                                    <li id="menu-item-2381" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2381"><a href="https://themebing.com/wp/tijarah/team/">Team</a></li>
                                    <li id="menu-item-2382" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2382"><a href="https://themebing.com/wp/tijarah/portfolio/">Portfolio</a></li>
                                    <li id="menu-item-2364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2364"><a href="https://themebing.com/wp/tijarah/faq/">Faq</a></li>
                                    <li id="menu-item-570" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-570"><a href="https://themebing.com/wp/tijarah/blog/">Blog</a></li>
                                    <li id="menu-item-1958" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1958"><a href="https://themebing.com/wp/tijarah/blog/the-best-advices-to-start-your-own-project/">Blog Details</a></li>
                                    <li id="menu-item-2383" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2383"><a href="https://themebing.com/wp/tijarah/404">404</a></li>
                                </ul> </li>
                            <li id="menu-item-290" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-283 current_page_item menu-item-290"><a href="https://themebing.com/wp/tijarah/contact/" aria-current="page">Contact</a></li>
                            <li class="menu-cart"> <a class="cart-contents menu-item" href="https://themebing.com/wp/tijarah/cart/" title="View your shopping cart"> <span class="cart-contents-count"><i class="fa fa-shopping-cart"></i> 0 items </span> </a>
                                <div class="mini-cart">
                                    <div class="widget woocommerce widget_shopping_cart">
                                        <div class="widget_shopping_cart_content"></div>
                                    </div>
                                </div> </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-xl-2 p-0 text-right">
                <div class="header-btn d-none d-xl-block">
                    <a class="my-account-btn" href="https://themebing.com/wp/tijarah/my-account/"> <img src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/images/user.png" alt="Contact"> My Account </a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- #masthead -->
<!--Mobile Navigation Toggler-->
<div class="off-canvas-menu-bar">
    <div class="container">
        <div class="row">
            <div class="col-8 my-auto">
                <a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah"></a>
            </div>
            <div class="col-2 my-auto">
                <div class="header-btn float-right">
                    <a class="my-account-btn" href="https://themebing.com/wp/tijarah/my-account/"> <img src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/images/user.png" alt="Contact"> </a>
                </div>
            </div>
            <div class="col-2 my-auto">
                <div class="mobile-nav-toggler">
                    <span class="fas fa-bars"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Mobile Menu  -->
<div class="off-canvas-menu">
    <div class="menu-backdrop"></div>
    <i class="close-btn fa fa-close"></i>
    <nav class="mobile-nav">
        <div class="text-center pt-3 pb-3">
            <a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah"></a>
        </div>
        <ul class="navigation">
            <!--Keep This Empty / Menu will come through Javascript-->
        </ul>
    </nav>
</div>
<!-- START BREADCRUMB AREA -->
<section class="breadcrumb-banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-title">
                    <h1> Contact </h1>
                    <div class="breadcrumbs">
                        <ul class="trail-items">
                            <li> <a href="https://themebing.com/wp/tijarah/" rel="home">Home</a> </li>
                            <li> Contact </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--END BREADCRUMB AREA-->
<div class="container">
    <div data-elementor-type="wp-page" data-elementor-id="283" class="elementor elementor-283" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-7197093 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="7197093" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-d993fa5" data-id="d993fa5" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-c1830da elementor-widget elementor-widget-google_maps" data-id="c1830da" data-element_type="widget" data-widget_type="google_maps.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-custom-embed">
                                                    <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=London%20Eye%2C%20London%2C%20United%20Kingdom&amp;t=m&amp;z=10&amp;output=embed&amp;iwloc=near" title="London Eye, London, United Kingdom" aria-label="London Eye, London, United Kingdom"></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-3a273a3" data-id="3a273a3" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-0c01ebd" data-id="0c01ebd" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-9e46632 elementor-widget elementor-widget-title" data-id="9e46632" data-element_type="widget" data-widget_type="title.default">
                                            <div class="elementor-widget-container">
                                                <div class="section-title text-center">
                                                    <h1>Get in touch</h1>
                                                    <p>Nemo enim ipsam voluptatem quia voluptas</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-cee02ec elementor-widget elementor-widget-shortcode" data-id="cee02ec" data-element_type="widget" data-widget_type="shortcode.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-shortcode">
                                                    <div role="form" class="wpcf7" id="wpcf7-f756-p283-o1" lang="en-US" dir="ltr">
                                                        <div class="screen-reader-response">
                                                            <p role="status" aria-live="polite" aria-atomic="true"></p>
                                                            <ul></ul>
                                                        </div>
                                                        <form action="/wp/tijarah/contact/#wpcf7-f756-p283-o1" method="post" class="wpcf7-form init" novalidate data-status="init">
                                                            <div style="display: none;">
                                                                <input type="hidden" name="_wpcf7" value="756">
                                                                <input type="hidden" name="_wpcf7_version" value="5.3.2">
                                                                <input type="hidden" name="_wpcf7_locale" value="en_US">
                                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f756-p283-o1">
                                                                <input type="hidden" name="_wpcf7_container_post" value="283">
                                                                <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                                                            </div>
                                                            <div class="contact-form">
                                                                <span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your name"></span>
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span>
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Subject"></span>
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap message"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your message"></textarea></span>
                                                                <br>
                                                                <input type="submit" value="Send message" class="wpcf7-form-control wpcf7-submit">
                                                            </div>
                                                            <div class="wpcf7-response-output" aria-hidden="true"></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-6b4355b elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6b4355b" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-82dea32" data-id="82dea32" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-ee0e5a7 elementor-widget elementor-widget-InfoBox_item" data-id="ee0e5a7" data-element_type="widget" data-widget_type="InfoBox_item.default">
                                            <div class="elementor-widget-container">
                                                <div class="infobox-item style-2 text-center" style="background: #FFFFFF">
                                                    <i aria-hidden="true" style="color:#fff" class="fas fa-mail-bulk"></i>
                                                    <h5 style="color:#000000">Email Address</h5>
                                                    <p style="color:#000000">info@example.com <br> moreinfo@email.com</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-d8d5fa2" data-id="d8d5fa2" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-25bba9d elementor-widget elementor-widget-InfoBox_item" data-id="25bba9d" data-element_type="widget" data-widget_type="InfoBox_item.default">
                                            <div class="elementor-widget-container">
                                                <div class="infobox-item style-2 text-center" style="background: #FFFFFF">
                                                    <i aria-hidden="true" style="color:#fff" class="fas fa-phone-volume"></i>
                                                    <h5 style="color:#000000">Phone Number</h5>
                                                    <p style="color:#000000">+44 569 985 9665 <br> +56 455 741 6982 </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-9c165eb" data-id="9c165eb" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-63d12ee elementor-widget elementor-widget-InfoBox_item" data-id="63d12ee" data-element_type="widget" data-widget_type="InfoBox_item.default">
                                            <div class="elementor-widget-container">
                                                <div class="infobox-item style-2 text-center" style="background: #FFFFFF">
                                                    <i aria-hidden="true" style="color:#fff" class="fas fa-map-marked-alt"></i>
                                                    <h5 style="color:#000000">Office Location</h5>
                                                    <p style="color:#000000">202 New Hampshire Avenue, Northwest #100, New York-2573</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-e3c427e elementor-section-full_width elementor-section-stretched elementor-section-height-default elementor-section-height-default" data-id="e3c427e" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-15c1e13" data-id="15c1e13" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-f6d8560 elementor-widget elementor-widget-call_to_action" data-id="f6d8560" data-element_type="widget" data-widget_type="call_to_action.default">
                                            <div class="elementor-widget-container">
                                                <section class="call-to-action">
                                                    <div class="container">
                                                        <div class="row justify-content-between">
                                                            <div class="col-md-7 text-left">
                                                                <h2>Join Us Today</h2>
                                                                <p class="mb-0">Over 75,000 designers and developers trust the DigiMarket.</p>
                                                            </div>
                                                            <div class="col-md-5 my-auto text-right">
                                                                <a class="tijarah-btn" href="#">Join Us Today</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<footer id="colophon" class="site-footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row justify-content-xl-between">
                <div class="col-lg-4">
                    <div id="custom_html-2" class="widget_text footer-widget widget_custom_html">
                        <div class="textwidget custom-html-widget">
                            <div class="pr-5">
                                <img width="140" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="img-fluid mb-4" alt="Tijarah">
                                <div class="footer-text mb-4">
                                    <p>Popularised in the with the release of etras sheets containing passages and more rcently with desop publishing software like Maker including.</p>
                                </div>
                                <div class="footer-social">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-2" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Products</h5>
                        <div class="menu-help-support-container">
                            <ul id="menu-help-support" class="menu">
                                <li id="menu-item-2271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2271"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li id="menu-item-2273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2273"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li id="menu-item-2275" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2275"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                                <li id="menu-item-2272" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-283 current_page_item menu-item-2272"><a href="https://themebing.com/wp/tijarah/contact/" aria-current="page">Contact Us</a></li>
                                <li id="menu-item-2274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2274"><a href="https://themebing.com/wp/tijarah/shop/">Plugins</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-3" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Resources</h5>
                        <div class="menu-our-company-container">
                            <ul id="menu-our-company" class="menu">
                                <li id="menu-item-2262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2262"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li id="menu-item-2268" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2268"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li id="menu-item-2270" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2270"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a></li>
                                <li id="menu-item-2266" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-283 current_page_item menu-item-2266"><a href="https://themebing.com/wp/tijarah/contact/" aria-current="page">Contact Us</a></li>
                                <li id="menu-item-2267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2267"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-4" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Company</h5>
                        <div class="menu-help-support-container">
                            <ul id="menu-help-support-1" class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2271"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2273"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2275"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-283 current_page_item menu-item-2272"><a href="https://themebing.com/wp/tijarah/contact/" aria-current="page">Contact Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2274"><a href="https://themebing.com/wp/tijarah/shop/">Plugins</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
                    <div id="nav_menu-5" class="footer-widget widget_nav_menu">
                        <h5 class="widget-title">Help and FAQs</h5>
                        <div class="menu-our-company-container">
                            <ul id="menu-our-company-1" class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2262"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2268"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2270"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-283 current_page_item menu-item-2266"><a href="https://themebing.com/wp/tijarah/contact/" aria-current="page">Contact Us</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2267"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-bar">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-7 text-center">
                    <p> </p>
                    <p>Copyright © 2020 tijarah All Rights Reserved.</p>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--======= Back to Top =======-->
<div id="backtotop">
    <i class="fa fa-lg fa-arrow-up"></i>
</div>
<script type="text/javascript">
    function fetch(){
        jQuery.ajax({
            url: 'https://themebing.com/wp/tijarah/wp-admin/admin-ajax.php',
            type: 'post',
            data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
            success: function(data) {
                if (jQuery('#keyword').val().length !== 0) {
                    jQuery('#datafetch').html( data );
                } else {
                    jQuery('#datafetch').html( '' );
                }

            }
        });

        jQuery("#datafetch").show();
    }
</script>
<script type="text/javascript">
    (function () {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })()
</script>
<script type="text/javascript" id="contact-form-7-js-extra">
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"https:\/\/themebing.com\/wp\/tijarah\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" id="contact-form-7-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/js/plugins.js?ver=1.2.7" id="tijarah-plugins-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/js/plugin.js?ver=1.2.7" id="tijarah-plugin-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70" id="jquery-blockui-js"></script>
<script type="text/javascript" id="wc-add-to-cart-js-extra">
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/themebing.com\/wp\/tijarah\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.8.0" id="wc-add-to-cart-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4" id="js-cookie-js"></script>
<script type="text/javascript" id="woocommerce-js-extra">
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.8.0" id="woocommerce-js"></script>
<script type="text/javascript" id="wc-cart-fragments-js-extra">
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_ad5ba321e4aaaebc185a7ab437321a4b","fragment_name":"wc_fragments_ad5ba321e4aaaebc185a7ab437321a4b","request_timeout":"5000"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.8.0" id="wc-cart-fragments-js"></script>
<script type="text/javascript" id="mailchimp-woocommerce-js-extra">
    /* <![CDATA[ */
    var mailchimp_public_data = {"site_url":"https:\/\/themebing.com\/wp\/tijarah","ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php","language":"en"};
    /* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min.js?ver=2.5.0" id="mailchimp-woocommerce-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/popper.min.js?ver=1.2.7" id="popper-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/bootstrap.min.js?ver=1.2.7" id="bootstrap-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/jquery.nice-select.min.js?ver=1.2.7" id="nice-select-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/plyr.min.js?ver=1.2.7" id="plyr-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/audio-player.js?ver=1.2.7" id="tijarah-audio-player-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/jquery.magnific-popup.min.js?ver=1.2.7" id="magnific-popup-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/skip-link-focus-fix.js?ver=1.2.7" id="tijarah-skip-link-focus-fix-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/main.js?ver=1.2.7" id="tijarah-main-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/wp-embed.min.js?ver=5.5.3" id="wp-embed-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.0.15" id="elementor-frontend-modules-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4" id="jquery-ui-position-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.8.1" id="elementor-dialog-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2" id="elementor-waypoints-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6" id="swiper-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.0.15" id="share-link-js"></script>
<script type="text/javascript" id="elementor-frontend-js-before">
    var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.15","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"https:\/\/themebing.com\/wp\/tijarah\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":283,"title":"Contact%20%E2%80%93%20Tijarah","excerpt":"","featuredImage":false}};
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.0.15" id="elementor-frontend-js"></script>
</body>
</html>
